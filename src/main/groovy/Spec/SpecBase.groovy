package spec

import groovy.transform.Canonical

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/09/17
 * Time: 11:43
 * To change this template use File | Settings | File Templates.
 */
@Canonical
class SpecBase {
    def value
    def description

    @Override
    String toString() {
        return description
    }

    Map values() {
        getValidProperties().collectEntries{[it.name, it.getProperty(this)]}
    }

    Map randomValues() {
        def ps = getValidProperties()
        def result = ps[new Random().nextInt(ps.size())]
        [(result.name):result.getProperty(this)]
    }

    Map values(Closure cls) {
        getValidProperties().findAll{cls.call(it)}.collectEntries{[it.name, it.getProperty(this)]}
    }

    private ArrayList<MetaProperty> getValidProperties() {
        this.metaClass.properties.findAll{ it.name != "class" && it.getProperty(this) != null }
    }
}
