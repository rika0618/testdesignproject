package spec
/**
 * Created with IntelliJ IDEA.
 * User: rika
 * Date: 13/09/11
 * Time: 10:52
 * To change this template use File | Settings | File Templates.
 */
class Mode extends SpecBase{
    def static AcceptMoney = new Mode(description: "貨幣受け入れ可能");
    def static AcceptingMoney = new Mode(description: "貨幣受け入れ処理中");
    def static SendingProduct= new Mode(description: "商品送出中");
    def static NotSaleTime = new Mode(description: "稼動停止時間中");
    def static Maintenance = new Mode(description: "メンテナンス中");
    def static Drawing = new Mode(description: "懸賞中");
    def static Refunding = new Mode(description: "返金中");

    static acceptMoneyModes = [AcceptMoney]
    static notAcceptMoneyModes = [AcceptingMoney, SendingProduct, NotSaleTime, Maintenance, Drawing, Refunding]

    def static rnd = new Random()
    static Mode randomAcceptMoneyMode() {
        AcceptMoney
    }

    static Mode randomNotAcceptMoneyMode() {
        notAcceptMoneyModes[rnd.nextInt(notAcceptMoneyModes.size() -1 )]
    }
}
