package spec

import groovy.transform.Canonical

import static spec.BillSpec.*

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/09/17
 * Time: 11:17
 * To change this template use File | Settings | File Templates.
 */

class BillSlot extends SpecBase{
    static String description = "紙幣投入口"
    static normalBillTypes = [_1000]
    static errorBillTypes = [_2000, _5000, _10000, nonstandardBill]

    def static rnd = new Random()
    static BillSpec randomNormalBill() {
        _1000
    }
    static BillSpec randomErrorBill() {
        errorBillTypes[rnd.nextInt(errorBillTypes.size() -1 )]
    }

    @Canonical
    static class Pools {
        def _1000
    }

    static int insertLimit(BillSpec bill) {
        switch (bill) {
            case _1000:
                1
                break
            default:
                throw new Exception()
                break
        }
    }

    static int storeLimit(BillSpec bill) {
        switch (bill) {
            case _1000:
                1000
                break
            default:
                throw new Exception()
                break
        }
    }
}
