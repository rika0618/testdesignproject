package spec

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/07
 * Time: 9:55
 * To change this template use File | Settings | File Templates.
 */
class Rack extends SpecBase{
    BigDecimal rightTempHigh
    BigDecimal rightTempLow

    def static coldRack = new Rack(description: "冷商品ラック", rightTempHigh: 6, rightTempLow: 1)
    def static hotRack = new Rack(description: "温商品ラック", rightTempHigh: 58, rightTempLow: 52)
}
