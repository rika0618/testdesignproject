package spec

/**
 * Created with IntelliJ IDEA.
 * User: k.kobayashi
 * Date: 13/09/19
 * Time: 11:41
 * To change this template use File | Settings | File Templates.
 */
class CoinSpec extends SpecBase {

    {
        description = ""
    }
    static CoinSpec _1 = new CoinSpec(value:1)
    static CoinSpec _5 = new CoinSpec(value:5)
    static CoinSpec _10 = new CoinSpec(value:10)
    static CoinSpec _50 = new CoinSpec(value:50)
    static CoinSpec _100 = new CoinSpec(value:100)
    static CoinSpec _500 = new CoinSpec(value:500)
    static CoinSpec nonstandardCoin = new CoinSpec(value:"ウォン")

    @Override
    String toString() {
        value instanceof Integer ? value + "円" : value + "(規格外)"
    }
}
