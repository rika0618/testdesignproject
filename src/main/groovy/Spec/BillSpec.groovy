package spec

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/09/20
 * Time: 9:36
 * To change this template use File | Settings | File Templates.
 */
class BillSpec extends SpecBase {

    {
        description = ""
    }
    static BillSpec _1000 = new BillSpec(value: 1000)
    static BillSpec _2000 = new BillSpec(value: 2000)
    static BillSpec _5000 = new BillSpec(value: 5000)
    static BillSpec _10000 = new BillSpec(value: 10000)
    static BillSpec nonstandardBill = new BillSpec(value: "ウォン")

    @Override
    String toString() {
        value instanceof Integer ? value + "円" : value + "(規格外)"
    }
}
