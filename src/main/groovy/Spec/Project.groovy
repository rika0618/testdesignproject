package spec

import resource.Cost

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/03
 * Time: 10:45
 * To change this template use File | Settings | File Templates.
 */

// TODO リソースに移す
class Project extends SpecBase {
    static def description = "プロジェクト全体"
    Cost cost
    Date startDate
    Date endDate
    static Whole = new Project(cost: new Cost(point: 300), startDate: new Date(), endDate: new Date())
    def static sprints = [
            new Project(description: "Sprint1", cost: new Cost(point: 40), startDate: new Date(), endDate: new Date()),
            new Project(description: "Sprint2", cost: new Cost(point: 40), startDate: new Date(), endDate: new Date()),
            new Project(description: "Sprint3", cost: new Cost(point: 40), startDate: new Date(), endDate: new Date()),
            new Project(description: "Sprint4", cost: new Cost(point: 40), startDate: new Date(), endDate: new Date())
    ]

    boolean validate() {
        //TODO 各Sprintの日付整合性やコストの整合性を検査する
        true
    }
}
