package spec

import groovy.transform.Immutable

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/09/17
 * Time: 11:46
 * To change this template use File | Settings | File Templates.
 */
@Immutable
class SpecWildCard {
    def static __ = new SpecWildCard()
    boolean equals(Object obj) {
        true
    }
}
