package spec

/**
 * Created with IntelliJ IDEA.
 * User: k.kobayashi
 * Date: 13/09/24
 * Time: 18:00
 * To change this template use File | Settings | File Templates.
 */
class Temperature extends SpecBase{
    def static High = new Temperature(value:45)
    def static Low = new Temperature(value:-45)
}
