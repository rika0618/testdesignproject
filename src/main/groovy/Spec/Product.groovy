package spec

/**
 * Created with IntelliJ IDEA.
 * User: rika
 * Date: 13/10/06
 * Time: 23:30
 * To change this template use File | Settings | File Templates.
 */
class Product extends SpecBase {
    Product(){
        description = ""
    }
    BigDecimal temperature

    static Product _250mlCan = new Product(value:"250mlCan")
    static Product _350mlCan = new Product(value:"350mlCan")
    static Product _500mlCan = new Product(value:"500mlCan")
    static Product _500mlPet = new Product(value:"500mlPet")

    @Override
    String toString(){
       description
    }
}
