package spec

/**
 * Created with IntelliJ IDEA.
 * User: rika
 * Date: 13/10/06
 * Time: 23:53
 * To change this template use File | Settings | File Templates.
 */
class Adaptability extends SpecBase {
    def static ColdProductTemperatureAdaptability = new Adaptability(
            description: "冷たい商品の温度",
            value: { 冷商品温度 -> (Rack.coldRack.rightTempLow <= 冷商品温度 && 冷商品温度 <= Rack.coldRack.rightTempHigh + 2) })
}
