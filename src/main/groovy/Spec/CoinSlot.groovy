package spec

import groovy.transform.Canonical

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/09/12
 * Time: 16:16
 * To change this template use File | Settings | File Templates.
 */

import static spec.CoinSpec.*
class CoinSlot extends SpecBase {
    static String description = "硬貨投入口"
    static normalCoinTypes = [_10, _50, _100, _500]
    static errorCoinTypes = [_1, _5, nonstandardCoin]

    def static rnd = new Random()
    static CoinSpec randomNormalCoin() {
        normalCoinTypes[rnd.nextInt(normalCoinTypes.size() -1 )]
    }
    static CoinSpec randomErrorCoin() {
        errorCoinTypes[rnd.nextInt(errorCoinTypes.size() -1 )]
    }
    @Canonical
    static class Pools {
        int _10
        int _50
        int _100
        int _500
    }

    static int insertLimit(CoinSpec coin) {
        switch (coin){
            case [_10, _50, _100]:
                20
                break
            case _500:
                10
                break
            default:
                throw new Exception()
                break
        }
    }

    static boolean hasAbilityOfBurn(Temperature temperature){
        new BurnTemperature().value < temperature.value
    }
}
