package resource.environment

/**
 * Created with IntelliJ IDEA.
 */
public enum Layout {
    Production, Staging, Development
}