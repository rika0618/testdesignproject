package resource.environment

import resource.Cost
import resource.TimeSpan

/**
 * Created with IntelliJ IDEA.
 */
class RuntimeInfrastructure implements Environment{
    TimeSpan timeSpan
    Cost cost
    Layout layout
}
