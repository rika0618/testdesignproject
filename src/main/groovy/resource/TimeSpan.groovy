package resource

import jp.xet.baseunits.time.BusinessCalendar
import jp.xet.baseunits.time.CalendarDate
import jp.xet.baseunits.time.CalendarInterval

/**
 * Created with IntelliJ IDEA.
 */
class TimeSpan {
    Date start
    Date end
    int workingDays(){
        new BusinessCalendar().getElapsedBusinessDays(CalendarInterval.inclusive(CalendarDate.from(start, TimeZone.default), CalendarDate.from(end, TimeZone.default)));
    }
}
