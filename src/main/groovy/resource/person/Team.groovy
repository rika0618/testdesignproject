package resource.person

/**
 * Created with IntelliJ IDEA.
 */
class Team {
    def member(){
        this.metaClass.properties.findAll{it.type == Person}.collect{it.getProperty(this)}
    }
}
