package resource.person

import resource.Cost
import resource.TimeSpan

/**
 * Created with IntelliJ IDEA.
 */
class Person {
    List<Knowledge> skillSet
    TimeSpan timeSpan
    Cost cost
}
