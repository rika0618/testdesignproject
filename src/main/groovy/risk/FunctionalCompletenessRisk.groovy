package risk

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/08
 * Time: 15:56
 * To change this template use File | Settings | File Templates.
 */
class FunctionalCompletenessRisk {
    static class CannotInsertMoney{}
    static class CannotKnowInsertedPrice{}
    static class CannotGetChange{}
    static class CannotGetProduct{}
    static class GetAnotherProduct{}
    static class CannotWinInDrawing{}
    static class CannotGetWinProduct{}
    static class CannotDraw{}
    static class CannotSupplyProduct{}
    static class CannotSupplyChange{}
    static class CannotGetOutMoney{}
}

class FunctionalCorrectnessRisk{
    static class GetLessChange{}
    static class RecognizedMoneyIsDifferentFromInsertMoney{}
    static class GetTepidProduct{}
}

class FunctionalAppropriateness{
    static class CannotKnowWhetherPushedButton{}
    static class CannotKnowWhetherDraw{}
    static class CloggedWithMoney{}
    static class CloggedWithProduct{}
}
