package risk

import spec.BillSlot
import spec.CoinSlot
import spec.SpecBase

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/02
 * Time: 14:08
 * To change this template use File | Settings | File Templates.
 */

class Risk {
    String name
    String type //いくつかから選びたいけどとりあえずString
    BigDecimal possibility //0.1, 0.3, 0.5, 0.7, 0.9 の5つから選ぶ
    BigDecimal severity //0.05, 0.1, 0.2, 0.4, 0.8 の5つから選ぶ
    List<Class> impactedSpecs
    String solutionType //いくつかから選びたいけどとりあえずString

    String description() {
        name + ", 分類 : " + type + ", リスクポイント : " + riskPoint() + ", 影響範囲 : " + impactedSpecsForDescription() + ", 対策タイプ : " + solutionType
    }

    BigDecimal riskPoint(){
        possibility * severity
    }

    String impactedSpecsForDescription(){
        (impactedSpecs*.description).join("*")
    }

    def riskAssertCondition(actual, expected){
        assert impactedSpecs.any{ actual.class == it}
    }

    protected Boolean Assert(actual, expected, Closure assertClosure = {a, e -> false}){
        riskAssertCondition(actual, expected)
        assert assertClosure(actual, expected)
        true
    }
    def methodMissing(String name, args){
        println "$name $args"
        return Assert(*args)
    }
    List<Object> operation(List<Closure> operations){
        operations.collect{it.call()}
    }
}