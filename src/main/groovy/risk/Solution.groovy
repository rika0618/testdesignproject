package risk

import groovy.transform.Canonical
import jp.xet.baseunits.util.spec.Specification
import resource.ResourceBase

/**
 * Created with IntelliJ IDEA.
 */
@Canonical
class Solution {
    String name
    Object solutionItem //今のところTestsかResource
}


class TestSolution extends Solution {
    String name
    Specification test
}

class ResourceSolution extends Solution {
    String name
    ResourceBase resource
}