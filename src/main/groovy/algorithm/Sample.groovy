package algorithm

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/09/26
 * Time: 13:35
 * To change this template use File | Settings | File Templates.
 */

class Parameter {
    String factor
    List level
}

class Combination extends AlgorithmBase{
    static String name = "組み合わせテスト"
    static enum Technique {
        All {
            @Override
            String value() { "全網羅" }
            @Override
            List<Map> generate(List<Parameter> parameters) {
                def ps = parameters*.level.combinations()
                ps.collect{List testCase ->
                    def index = 0
                    testCase.collectEntries{[parameters[index++].factor, it]}
                }
            }
        },
        KWay {
            @Override
            String value() { "k-way" }
            @Override
            List<Map> generate(List<Parameter> parameters) {
                []
            }
        },
        Once {
            @Override
            String value() { "全てが1回以上出現" }
            @Override
            List<Map> generate(List<Parameter> parameters) {
                []
            }
        },
        NSwitch{
            @Override
            String value() { "Nスイッチ" }
            @Override
            List<Map> generate(List<Parameter> parameters) {
                []
            }
        };
        abstract List<Map> generate(List<Parameter> parameters)
        abstract String value()

    }
}
