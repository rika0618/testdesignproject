package algorithm

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/09/26
 * Time: 17:48
 * To change this template use File | Settings | File Templates.
 */
class Compromise {
    Map<String, Integer> shouldItem
    List<String> useItem
    int rate(){
        shouldItem.findAll {useItem.contains(it.key)}*.getValue().sum() / shouldItem*.getValue().sum() * 100
    }
}

class HogeCompromise extends Compromise {
    static should = ["Apacheサーバー":1, "クライアント認証使用":2, "リバプロ構成一致":1, "OSバージョン一致":1]
    static Compromise High = new Compromise(shouldItem:should, useItem: ["Apacheサーバー", "クライアント認証使用"])
    static Compromise Normal = new Compromise(shouldItem:should, useItem: ["Apacheサーバー", "OSバージョン一致"])
    static Compromise Low = new Compromise(shouldItem:should, useItem: ["Apacheサーバー", ])
}
