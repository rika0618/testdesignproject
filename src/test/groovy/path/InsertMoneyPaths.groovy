package path

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/09/24
 * Time: 10:22
 * To change this template use File | Settings | File Templates.
 */
class InsertMoneyPaths {
    enum AcceptPath {
        Accept {
            @Override
            String value() { "受け入れ処理が行われる" }
        },
        Refund {
            @Override
            String value() { "返金処理が行われる" }
        }
    }

    enum PoolMoneyIndicatorPath {
        NotChange {
            @Override
            String value() { "変化なし" }
        },
        AddMoney {
            @Override
            String value() { "投入された貨幣の金額が加算される" }
        }
    }

    enum RefundPath {
        NotRefund {
            @Override
            String value() { "返金されない" }
        },
        RefundFromBillSlot {
            @Override
            String value() { "紙幣投入口から返金される" }
        },
        RefundFromChangeSlot {
            @Override
            String value() { "つり銭取り出し口から返金される" }
        },
        RefundFromBillSlotAndChangeSlot {
            @Override
            String value() { "紙幣投入口とつり銭取り出し口から返金される" }
        }
    }

    enum CoinTypePath {
        AcceptCoin {
            @Override
            String value() { "利用可能硬貨" }
        },
        NotAcceptCoin {
            @Override
            String value() { "利用不可能硬貨" }
        }
    }

    enum BillTypePath {
        AcceptBill {
            @Override
            String value() { "利用可能紙幣" }
        },
        NotAcceptBill {
            @Override
            String value() { "利用不可能紙幣" }
        }
    }

    enum InsertLimitNumberPath {
        LessThanInsertLimitNumber {
            @Override
            String value() { "投入済枚数が上限より少ない" }
        },
        MoreThanInsertLimitNumber {
            @Override
            String value() { "投入済枚数が上限以上" }
        }
    }

    enum CoinPoolStatePath {
        NoCoinInserted {
            @Override
            String value() { "どの硬貨も投入されていない" }
        },
        InsertableAllCoin {
            @Override
            String value() { "どの硬貨もあと1枚投入可能" }
        },
        MaxExcludeCoin {
            @Override
            String value() { "該当硬貨のみ1枚投入可能" }
        },
        MaxAllCoin {
            @Override
            String value() { "どの硬貨も投入不可能" }
        }
    }

    enum BillPoolStatePath {
        NoBillInserted {
            String value() { "どの紙幣も投入されていない" }
        },
        InsertableAllBill {
            @Override
            String value() { "どの紙幣もあと1枚投入可能" }
        },
        MaxExcludeBill {
            @Override
            String value() { "該当紙幣のみ1枚投入可能" }
        },
        MaxAllBill {
            @Override
            String value() { "どの紙幣も投入不可能" }
        }
    }

    enum StoreLimitNumberPath {
        LessThanStoreLimitNumber {
            @Override
            String value() { "金庫内枚数が上限より少ない" }
        },
        MoreThanStoreLimitNumber {
            @Override
            String value() { "金庫内枚数が上限以上" }
        }
    }

    enum LimitPricePath {
        LessThanInsertLimitPrice {
            @Override
            String value() { "投入済金額が上限より少ない" }
        },
        MoreThanInsertLimitPrice {
            @Override
            String value() { "投入済金額が上限以上" }
        }
    }

    enum ModePath {
        AcceptMoneyMode {
            @Override
            String value() { "貨幣受け入れ可能" }
        },
        NotAcceptMoneyMode {
            @Override
            String value() { "貨幣受け入れ可能以外" }
        }
    }
}
