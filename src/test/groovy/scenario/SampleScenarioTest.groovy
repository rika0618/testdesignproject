package scenario

import spock.lang.Specification


/**
 * Created with IntelliJ IDEA.
 * User: k.kobayashi
 * Date: 13/09/24
 * Time: 17:21
 * To change this template use File | Settings | File Templates.
 */

class SampleScenarioTest extends Specification {



    def "小さい子が真夏にジュースを買ったときにやけどをしないこと"(){
        when:
        true
        then:"お金をいれるときに投入口にふれてもやけどをしない"
        and :"ジュースをとりだすときにふたと底にふれてもやけどをしない"
        and :"ジュースにふれてもやけどをしない"
    }

}