package piece

import spock.lang.Specification
import spock.lang.Unroll

import static spec.CoinSpec.*
import static spec.CoinSlot.*
import static spec.MaximumPrice.*
import static spec.Mode.*

import static status.CoinPoolStatus.*

import static path.InsertMoneyPaths.AcceptPath.*
import static path.InsertMoneyPaths.CoinTypePath.*
import static path.InsertMoneyPaths.InsertLimitNumberPath.*
import static path.InsertMoneyPaths.CoinPoolStatePath.*
import static path.InsertMoneyPaths.LimitPricePath.*
import static path.InsertMoneyPaths.ModePath.*

/**
 * Created with IntelliJ IDEA.
 * User: rika
 * Date: 13/09/10
 * Time: 11:01
 * To change this template use File | Settings | File Templates.
 */
class CoinSlotTests extends Specification {

    def setupSpec() {
        Boolean.metaClass.enable = {->
            delegate ? "できる" : "できない"
        }
    }

    @Unroll
    def "#inputPath -> #outputPath : #coin を受け入れることが #expected.enable()"() {
        given: "稼動モードが貨幣受け入れ可能である"
        when: "#coin を入れる"
        then: "#coinがAcceptCoinであれば #outputPath"
        and: "#coinがAcceptCoinでなければ #outputPath"
        where:
        inputPath             | outputPath     | coin            | expected
        AcceptCoin.value()    | Accept.value() | _10             | true
        AcceptCoin.value()    | Accept.value() | _50             | true
        AcceptCoin.value()    | Accept.value() | _100            | true
        AcceptCoin.value()    | Accept.value() | _500            | true
        NotAcceptCoin.value() | Refund.value() | _1              | false
        NotAcceptCoin.value() | Refund.value() | _5              | false
        NotAcceptCoin.value() | Refund.value() | nonstandardCoin | false
    }

    @Unroll
    def "#inputPath -> #outputPath : #coin を #insertedNumber 枚入れた状態でさらにもう1枚入れることが #expected.enable()"() {
        given: "稼動モードが貨幣受け入れ可能である"
        and: "#coin を #insertedNumber 枚まで入れる"
        when: "#coin を入れる"
        then: "#insertedNumber が投入上限枚数より少ないとき #outputPath"
        and: "#insertedNumber が投入上限以上のとき #outputPath"
        where:
        inputPath                         | outputPath     | coin | insertedNumber        | expected
        LessThanInsertLimitNumber.value() | Accept.value() | _10  | insertLimit(_10) - 1  | true
        LessThanInsertLimitNumber.value() | Accept.value() | _50  | insertLimit(_50) - 1  | true
        LessThanInsertLimitNumber.value() | Accept.value() | _100 | insertLimit(_100) - 1 | true
        LessThanInsertLimitNumber.value() | Accept.value() | _500 | insertLimit(_500) - 1 | true
        MoreThanInsertLimitNumber.value() | Refund.value() | _10  | insertLimit(_10)      | false
        MoreThanInsertLimitNumber.value() | Refund.value() | _50  | insertLimit(_50)      | false
        MoreThanInsertLimitNumber.value() | Refund.value() | _100 | insertLimit(_100)     | false
        MoreThanInsertLimitNumber.value() | Refund.value() | _500 | insertLimit(_500)     | false
    }

    @Unroll
    def "#inputPath -> #outputPath : #poolState の状態で #coin を入れることが #expected.enable()"() {
        given: "稼動モードが貨幣受け入れ可能である"
        and: "投入済硬貨を #poolState にしておく"
        when: "#coin を入れる"
        then: "#poolState が どの硬貨もあと1枚投入可能, 指定された硬貨のみ1枚投入可能 のとき #outputPath"
        and: "#poolState が どの硬貨も入らない のとき #outputPath"
        where:
        inputPath                 | outputPath     | coin | poolState                | expected
        InsertableAllCoin.value() | Accept.value() | _10  | insertableAllCoinPools() | true
        InsertableAllCoin.value() | Accept.value() | _50  | insertableAllCoinPools() | true
        InsertableAllCoin.value() | Accept.value() | _100 | insertableAllCoinPools() | true
        InsertableAllCoin.value() | Accept.value() | _500 | insertableAllCoinPools() | true
        MaxExcludeCoin.value()    | Accept.value() | _10  | maxExcludeCoinPool(_10)  | true
        MaxExcludeCoin.value()    | Accept.value() | _50  | maxExcludeCoinPool(_50)  | true
        MaxExcludeCoin.value()    | Accept.value() | _100 | maxExcludeCoinPool(_100) | true
        MaxExcludeCoin.value()    | Accept.value() | _500 | maxExcludeCoinPool(_500) | true
        MaxAllCoin.value()        | Refund.value() | _10  | maxAllCoinPools()        | false
        MaxAllCoin.value()        | Refund.value() | _50  | maxAllCoinPools()        | false
        MaxAllCoin.value()        | Refund.value() | _100 | maxAllCoinPools()        | false
        MaxAllCoin.value()        | Refund.value() | _500 | maxAllCoinPools()        | false
    }

    @Unroll
    def "#inputPath -> #outputPath : #insertedPrice 円貨幣を投入した状態で #coin を 受け入れることが #expected.enable()"() {
        given: "稼動モードが貨幣受け入れ可能である"
        and: "#coin を #insertedPrice 円まで入れる"
        when: "#coin を入れる"
        then: "#insertedPrice が投入上限金額より少ないとき #outputPath"
        and: "#insertedPrice が投入上限金額以上のとき #outputPath"
        where:
        inputPath                        | outputPath     | coin | insertedPrice             | expected
        LessThanInsertLimitPrice.value() | Accept.value() | _10  | maximumPrice - _10.value  | true
        LessThanInsertLimitPrice.value() | Accept.value() | _50  | maximumPrice - _50.value  | true
        LessThanInsertLimitPrice.value() | Accept.value() | _100 | maximumPrice - _100.value | true
        LessThanInsertLimitPrice.value() | Accept.value() | _500 | maximumPrice - _500.value | true
        MoreThanInsertLimitPrice.value() | Refund.value() | _10  | maximumPrice              | false
        MoreThanInsertLimitPrice.value() | Refund.value() | _50  | maximumPrice              | false
        MoreThanInsertLimitPrice.value() | Refund.value() | _100 | maximumPrice              | false
        MoreThanInsertLimitPrice.value() | Refund.value() | _500 | maximumPrice              | false
    }

    @Unroll
    def "#inputPath -> #outputPath : #mode のとき #coin を受け入れることが #expected.enable()"() {
        given: "稼動モードが #mode である"
        when: "#coin を入れる"
        then: "#mode が貨幣受け入れ可能のとき #outputPath"
        and: "#mode が貨幣受け入れ可能でないとき #outputPath"
        where:
        inputPath                  | outputPath     | coin | mode           | expected
        AcceptMoneyMode.value()    | Accept.value() | _10  | AcceptMoney    | true
        AcceptMoneyMode.value()    | Accept.value() | _50  | AcceptMoney    | true
        AcceptMoneyMode.value()    | Accept.value() | _100 | AcceptMoney    | true
        AcceptMoneyMode.value()    | Accept.value() | _500 | AcceptMoney    | true
        NotAcceptMoneyMode.value() | Refund.value() | _10  | AcceptingMoney | false
        NotAcceptMoneyMode.value() | Refund.value() | _50  | AcceptingMoney | false
        NotAcceptMoneyMode.value() | Refund.value() | _100 | AcceptingMoney | false
        NotAcceptMoneyMode.value() | Refund.value() | _500 | AcceptingMoney | false
        NotAcceptMoneyMode.value() | Refund.value() | _10  | SendingProduct | false
        NotAcceptMoneyMode.value() | Refund.value() | _50  | SendingProduct | false
        NotAcceptMoneyMode.value() | Refund.value() | _100 | SendingProduct | false
        NotAcceptMoneyMode.value() | Refund.value() | _500 | SendingProduct | false
        NotAcceptMoneyMode.value() | Refund.value() | _10  | NotSaleTime    | false
        NotAcceptMoneyMode.value() | Refund.value() | _50  | NotSaleTime    | false
        NotAcceptMoneyMode.value() | Refund.value() | _100 | NotSaleTime    | false
        NotAcceptMoneyMode.value() | Refund.value() | _500 | NotSaleTime    | false
        NotAcceptMoneyMode.value() | Refund.value() | _10  | Maintenance    | false
        NotAcceptMoneyMode.value() | Refund.value() | _50  | Maintenance    | false
        NotAcceptMoneyMode.value() | Refund.value() | _100 | Maintenance    | false
        NotAcceptMoneyMode.value() | Refund.value() | _500 | Maintenance    | false
        NotAcceptMoneyMode.value() | Refund.value() | _10  | Drawing        | false
        NotAcceptMoneyMode.value() | Refund.value() | _50  | Drawing        | false
        NotAcceptMoneyMode.value() | Refund.value() | _100 | Drawing        | false
        NotAcceptMoneyMode.value() | Refund.value() | _500 | Drawing        | false
        NotAcceptMoneyMode.value() | Refund.value() | _10  | Refunding      | false
        NotAcceptMoneyMode.value() | Refund.value() | _50  | Refunding      | false
        NotAcceptMoneyMode.value() | Refund.value() | _100 | Refunding      | false
        NotAcceptMoneyMode.value() | Refund.value() | _500 | Refunding      | false
    }
}
