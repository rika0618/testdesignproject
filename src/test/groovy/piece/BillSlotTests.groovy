package piece

import spock.lang.Specification
import spock.lang.Unroll

import static spec.BillSpec.*
import static spec.BillSlot.*
import static spec.MaximumPrice.*
import static spec.Mode.*

import static path.InsertMoneyPaths.AcceptPath.*
import static path.InsertMoneyPaths.BillTypePath.*
import static path.InsertMoneyPaths.InsertLimitNumberPath.*
import static path.InsertMoneyPaths.StoreLimitNumberPath.*
import static path.InsertMoneyPaths.LimitPricePath.*
import static path.InsertMoneyPaths.ModePath.*

/**
 * Created with IntelliJ IDEA.
 * User: rika
 * Date: 13/09/10
 * Time: 14:51
 * To change this template use File | Settings | File Templates.
 */
class BillSlotTests extends Specification {

    def setupSpec() {
        Boolean.metaClass.enable = {->
            delegate ? "できる" : "できない"
        }
    }

    @Unroll
    def "#inputPath -> #outputPath : #bill を受け入れることが #expected.enable()"() {
        given: "稼動状態が貨幣受け入れ可能である"
        when: "#billを入れる"
        then: "#billが利用可能紙幣であれば #outputPath"
        and: "#billが利用可能紙幣でなければ #outputPath"
        where:
        inputPath             | outputPath     | bill            | expected
        AcceptBill.value()    | Accept.value() | _1000           | true
        NotAcceptBill.value() | Refund.value() | _2000           | false
        NotAcceptBill.value() | Refund.value() | _5000           | false
        NotAcceptBill.value() | Refund.value() | _10000          | false
        NotAcceptBill.value() | Refund.value() | nonstandardBill | false
    }

    @Unroll
    def "#inputPath -> #outputPath : #bill を #insertedNumber 枚入れた状態でさらにもう1枚入れることが #expected.enable()"() {
        given: "稼動状態が貨幣受け入れ可能である"
        and: "#bill を #insertedNumber 枚まで入れる"
        when: "#bill を入れる"
        then: "#insertedNumber が投入上限枚数より少ないときプールに入り受け入れ処理が行われる"
        and: "#insertedNumber が投入上限以上のときプールに入らずに返金処理が行われる"
        where:
        inputPath                         | outputPath     | bill  | insertedNumber         | expected
        LessThanInsertLimitNumber.value() | Accept.value() | _1000 | insertLimit(_1000) - 1 | true
        MoreThanInsertLimitPrice.value()  | Refund.value() | _1000 | insertLimit(_1000)     | false
    }

    @Unroll
    def "#inputPath -> #outputPath : #bill が金庫内に #storeNumber 枚ある状態でさらにもう1枚入れることが #expected.enable()"() {
        given: "稼動状態が貨幣受け入れ可能である"
        and: "#bill を #storeNumber 枚まで入れる"
        when: "#bill を入れる"
        then: "#storeNumber が投入上限枚数より少ないときプールに入り受け入れ処理が行われる"
        and: "#storeNumber が投入上限以上のときプールに入らずに返金処理が行われる"
        where:
        inputPath                        | outputPath     | bill  | storeNumber           | expected
        LessThanStoreLimitNumber.value() | Accept.value() | _1000 | storeLimit(_1000) - 1 | true
        MoreThanStoreLimitNumber.value() | Refund.value() | _1000 | storeLimit(_1000)     | false
    }

    @Unroll
    def "#inputPath -> #outputPath : #insertedPrice 円貨幣を投入した状態で #bill を 受け入れることが #expected.enable()"() {
        given: "稼動状態が貨幣受け入れ可能である"
        and: "#bill を #insertedPrice 円まで入れる"
        when: "#bill を入れる"
        then: "#insertedPrice が投入上限金額より少ないときプールに入る"
        and: "#insertedPrice が投入上限金額以上のときプールに入らずに返金処理が行われる"
        where:
        inputPath                        | outputPath     | bill  | insertedPrice              | expected
        LessThanInsertLimitPrice.value() | Accept.value() | _1000 | maximumPrice - _1000.value | true
        MoreThanInsertLimitPrice.value() | Refund.value() | _1000 | maximumPrice               | false
    }

    @Unroll
    def "#inputPath -> #outputPath : #mode のとき #bill を受け入れることが #expected.enable()"() {
        given: "稼動モードが #mode である"
        when: "#bill を入れる"
        then: "#mode が貨幣受け入れ可能のときプールに入り受け入れ処理が行われる"
        and: "#mode が貨幣受け入れ可能でないときプールに入らず返金処理が行われる"
        where:
        inputPath                  | outputPath     | bill  | mode           | expected
        AcceptMoneyMode.value()    | Accept.value() | _1000 | AcceptMoney    | true
        NotAcceptMoneyMode.value() | Refund.value() | _1000 | SendingProduct | false
        NotAcceptMoneyMode.value() | Refund.value() | _1000 | NotSaleTime    | false
        NotAcceptMoneyMode.value() | Refund.value() | _1000 | Maintenance    | false
        NotAcceptMoneyMode.value() | Refund.value() | _1000 | Drawing        | false
        NotAcceptMoneyMode.value() | Refund.value() | _1000 | Refunding      | false
    }
}
