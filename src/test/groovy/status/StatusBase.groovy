package status

import groovy.transform.Canonical

/**
 * Created with IntelliJ IDEA.
 * User: rika
 * Date: 13/09/18
 * Time: 23:41
 * To change this template use File | Settings | File Templates.
 */
@Canonical
class StatusBase {
    def value
    def description

    @Override
    String toString(){
        return description
    }
}
