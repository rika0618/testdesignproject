package status

import spec.BillSlot

/**
 * Created with IntelliJ IDEA.
 * User: rika
 * Date: 13/09/19
 * Time: 0:07
 * To change this template use File | Settings | File Templates.
 */
class BillPoolStatus extends StatusBase {
    static def insertableAllBillPools = {->
        def pools = new BillSlot.Pools(*(getAcceptNormalBill().collect{BillSlot.insertLimit(it) - 1}))
        new BillPoolStatus(value:pools, description: "どの紙幣もあと1枚投入可能")
    }

    static def maxExcludeBillPool = { exclude ->
        def pools = new BillSlot.Pools(*(getAcceptNormalBill().collect{it == exclude ? BillSlot.insertLimit(it) - 1 : BillSlot.insertLimit(it)}))
        new BillPoolStatus(value: pools, description: "指定された紙幣のみ1枚投入可能")
    }

    static def maxAllBillPools = {->
        def pools = new BillSlot.Pools(*(getAcceptNormalBill().collect{BillSlot.insertLimit(it)}))
        new BillPoolStatus(value: pools, description: "どの紙幣も投入不可能")
    }

    static private getAcceptNormalBill() {
        BillSlot.normalBillTypes
    }

}
