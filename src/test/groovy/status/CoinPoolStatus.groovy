package status

import spec.CoinSlot

/**
 * Created with IntelliJ IDEA.
 * User: rika
 * Date: 13/09/18
 * Time: 23:37
 * To change this template use File | Settings | File Templates.
 */
class CoinPoolStatus extends StatusBase {
    static def insertableAllCoinPools = {->
        def pools = new CoinSlot.Pools(*(getAcceptNormalCoin().collect{CoinSlot.insertLimit(it) - 1}))
        new CoinPoolStatus(value: pools, description: "どの硬貨もあと1枚投入可能")
    }

    static def maxExcludeCoinPool = { exclude ->
        def pools = new CoinSlot.Pools(*(getAcceptNormalCoin().collect{it == exclude ? CoinSlot.insertLimit(it) - 1 : CoinSlot.insertLimit(it)}))
        new CoinPoolStatus(value: pools, description: "指定された硬貨のみ1枚投入可能")
    }

    static def maxAllCoinPools = {->
        def pools = new CoinSlot.Pools(*(getAcceptNormalCoin().collect{CoinSlot.insertLimit(it)}))
        new CoinPoolStatus(value: pools, description: "どの硬貨も投入不可能")
    }

    static private getAcceptNormalCoin() {
        CoinSlot.normalCoinTypes
    }
}
