package usecase

import groovy.transform.Canonical
import spock.lang.Specification
import spock.lang.Unroll

import spec.CoinSlot
import spec.BillSlot
import static spec.CoinSlot.*
import static spec.BillSlot.*
import static spec.CoinSpec.*
import static spec.BillSpec.*
import static spec.Mode.*
import static path.InsertMoneyPaths.AcceptPath.*
import static path.InsertMoneyPaths.PoolMoneyIndicatorPath.*
import static path.InsertMoneyPaths.RefundPath.*
import static path.InsertMoneyPaths.CoinTypePath.*
import static path.InsertMoneyPaths.BillTypePath.*
import static path.InsertMoneyPaths.ModePath.*

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/09/18
 * Time: 15:48
 * To change this template use File | Settings | File Templates.
 */
class InsertMoneyTests extends Specification {

    def setup() {
        Boolean.metaClass.enable = {->
            delegate ? "できる" : "できない"
        }
    }

    @Canonical
    static class MoneyPool {
        @Delegate
        CoinSlot.Pools coinPool
        @Delegate
        BillSlot.Pools billPool
    }

    // 仕様として返金がある
    class RefundMoney {

    }

    // テストタイプの一部としてAssertを抽象化する
    def AssertRefund(coinRefund, billRefund) {
    }

    //TODO 貨幣を受け付ける・貨幣の受付を中断する

    @Unroll
    def "【硬貨種網羅】#inputPath1 -> #inputPath2 -> #outputPath : #mode で #coin を入れると 金額表示機に #poolMoneyIndicator 、#refund"() {
        given: "稼動モードを#modeにする"
        and: "どの貨幣も投入されていない状態にする"
        and: "金額表示機が0を表示していることを確認する"
        when: "#coin を入れる"
        then: "#coin が 利用可能貨幣で #mode が貨幣受け入れ可能の場合、 金額表示機に表示されている金額に投入された硬貨の金額が加算され、返金されない"
        and: "#coin が 利用不可能貨幣 または #mode が貨幣受け入れ可能以外の場合、 金額表示機の表示に変化がなく、つり銭取り出し口から返金される"
        where:
        inputPath1            | inputPath2                 | outputPath     | coin            | mode                       | poolMoneyIndicator | refund
        AcceptCoin.value()    | AcceptMoneyMode.value()    | Accept.value() | _10             | randomAcceptMoneyMode()    | AddMoney.value()   | NotRefund.value()
        AcceptCoin.value()    | AcceptMoneyMode.value()    | Accept.value() | _50             | randomAcceptMoneyMode()    | AddMoney.value()   | NotRefund.value()
        AcceptCoin.value()    | AcceptMoneyMode.value()    | Accept.value() | _100            | randomAcceptMoneyMode()    | AddMoney.value()   | NotRefund.value()
        AcceptCoin.value()    | AcceptMoneyMode.value()    | Accept.value() | _500            | randomAcceptMoneyMode()    | AddMoney.value()   | NotRefund.value()
        NotAcceptCoin.value() | AcceptMoneyMode.value()    | Refund.value() | _1              | randomAcceptMoneyMode()    | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptCoin.value() | AcceptMoneyMode.value()    | Refund.value() | _5              | randomAcceptMoneyMode()    | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptCoin.value() | AcceptMoneyMode.value()    | Refund.value() | nonstandardCoin | randomAcceptMoneyMode()    | NotChange.value()  | RefundFromChangeSlot.value()
        AcceptCoin.value()    | NotAcceptMoneyMode.value() | Refund.value() | _10             | randomNotAcceptMoneyMode() | NotChange.value()  | RefundFromChangeSlot.value()
        AcceptCoin.value()    | NotAcceptMoneyMode.value() | Refund.value() | _50             | randomNotAcceptMoneyMode() | NotChange.value()  | RefundFromChangeSlot.value()
        AcceptCoin.value()    | NotAcceptMoneyMode.value() | Refund.value() | _100            | randomNotAcceptMoneyMode() | NotChange.value()  | RefundFromChangeSlot.value()
        AcceptCoin.value()    | NotAcceptMoneyMode.value() | Refund.value() | _500            | randomNotAcceptMoneyMode() | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptCoin.value() | NotAcceptMoneyMode.value() | Refund.value() | _1              | randomNotAcceptMoneyMode() | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptCoin.value() | NotAcceptMoneyMode.value() | Refund.value() | _5              | randomNotAcceptMoneyMode() | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptCoin.value() | NotAcceptMoneyMode.value() | Refund.value() | nonstandardCoin | randomNotAcceptMoneyMode() | NotChange.value()  | RefundFromChangeSlot.value()
    }

    @Unroll
    def "【紙幣種網羅】#inputPath1 -> #inputPath2 -> #outputPath : #mode で #bill を入れると 金額表示機に #poolMoneyIndicator 、#refund"() {
        given: "稼動モードを#modeにする"
        and: "どの貨幣も投入されていない状態にする"
        and: "金額表示機が0を表示していることを確認する"
        when: "#bill を入れる"
        then: "#bill が 利用可能貨幣で #mode が貨幣受け入れ可能の場合、 金額表示機に表示されている金額に投入された紙幣の金額が加算され、返金されない"
        and: "#bill が 利用不可能貨幣 または #mode が貨幣受け入れ可能以外の場合、 金額表示機の表示に変化がなく、紙幣投入口から返金される"
        where:
        inputPath1            | inputPath2                 | outputPath     | bill            | mode                       | poolMoneyIndicator | refund
        AcceptBill.value()    | AcceptMoneyMode.value()    | Accept.value() | _1000           | randomAcceptMoneyMode()    | AddMoney.value()   | NotRefund.value()
        NotAcceptBill.value() | AcceptMoneyMode.value()    | Refund.value() | _2000           | randomAcceptMoneyMode()    | NotChange.value()  | RefundFromBillSlot.value()
        NotAcceptBill.value() | AcceptMoneyMode.value()    | Refund.value() | _5000           | randomAcceptMoneyMode()    | NotChange.value()  | RefundFromBillSlot.value()
        NotAcceptBill.value() | AcceptMoneyMode.value()    | Refund.value() | _10000          | randomAcceptMoneyMode()    | NotChange.value()  | RefundFromBillSlot.value()
        NotAcceptBill.value() | AcceptMoneyMode.value()    | Refund.value() | nonstandardBill | randomAcceptMoneyMode()    | NotChange.value()  | RefundFromBillSlot.value()
        AcceptBill.value()    | NotAcceptMoneyMode.value() | Refund.value() | _1000           | randomNotAcceptMoneyMode() | NotChange.value()  | RefundFromBillSlot.value()
        NotAcceptBill.value() | NotAcceptMoneyMode.value() | Refund.value() | _2000           | randomNotAcceptMoneyMode() | NotChange.value()  | RefundFromBillSlot.value()
        NotAcceptBill.value() | NotAcceptMoneyMode.value() | Refund.value() | _5000           | randomNotAcceptMoneyMode() | NotChange.value()  | RefundFromBillSlot.value()
        NotAcceptBill.value() | NotAcceptMoneyMode.value() | Refund.value() | _10000          | randomNotAcceptMoneyMode() | NotChange.value()  | RefundFromBillSlot.value()
        NotAcceptBill.value() | NotAcceptMoneyMode.value() | Refund.value() | nonstandardBill | randomNotAcceptMoneyMode() | NotChange.value()  | RefundFromBillSlot.value()
    }

    @Unroll
    def "【稼動モード網羅/硬貨】#inputPath1 -> #inputPath2 -> #outputPath : #mode で #coin を入れると 金額表示機に #poolMoneyIndicator 、#refund"() {
        given: "稼動モードを#modeにする"
        and: "どの貨幣も投入されていない状態にする"
        and: "金額表示機が0を表示していることを確認する"
        when: "#bill を入れる"
        then: "#bill が 利用可能貨幣で #mode が貨幣受け入れ可能の場合、 金額表示機に表示されている金額に投入された紙幣の金額が加算され、返金されない"
        and: "#bill が 利用不可能貨幣 または #mode が貨幣受け入れ可能以外の場合、 金額表示機の表示に変化がなく、紙幣投入口から返金される"
        where:
        inputPath1                 | inputPath2            | outputPath     | mode           | coin               | poolMoneyIndicator | refund
        AcceptMoneyMode.value()    | AcceptCoin.value()    | Accept.value() | AcceptMoney    | randomNormalCoin() | AddMoney.value()   | NotRefund.value()
        NotAcceptMoneyMode.value() | AcceptCoin.value()    | Accept.value() | AcceptingMoney | randomNormalCoin() | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | AcceptCoin.value()    | Accept.value() | SendingProduct | randomNormalCoin() | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | AcceptCoin.value()    | Accept.value() | NotSaleTime    | randomNormalCoin() | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | AcceptCoin.value()    | Refund.value() | Maintenance    | randomNormalCoin() | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | AcceptCoin.value()    | Refund.value() | Drawing        | randomNormalCoin() | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | AcceptCoin.value()    | Refund.value() | Refunding      | randomNormalCoin() | NotChange.value()  | RefundFromChangeSlot.value()
        AcceptMoneyMode.value()    | NotAcceptCoin.value() | Refund.value() | AcceptMoney    | randomErrorCoin()  | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | NotAcceptCoin.value() | Refund.value() | AcceptingMoney | randomErrorCoin()  | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | NotAcceptCoin.value() | Refund.value() | SendingProduct | randomErrorCoin()  | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | NotAcceptCoin.value() | Refund.value() | NotSaleTime    | randomErrorCoin()  | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | NotAcceptCoin.value() | Refund.value() | Maintenance    | randomErrorCoin()  | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | NotAcceptCoin.value() | Refund.value() | Drawing        | randomErrorCoin()  | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | NotAcceptCoin.value() | Refund.value() | Refunding      | randomErrorCoin()  | NotChange.value()  | RefundFromChangeSlot.value()
    }

    @Unroll
    def "【稼動モード網羅/紙幣】#inputPath1 -> #inputPath2 -> #outputPath : #mode で #bill を入れると 金額表示機に #poolMoneyIndicator 、#refund"() {
        given: "稼動モードを貨幣受け入れ可能にする"
        and: "どの貨幣も投入されていない状態にする"
        and: "金額表示機がを表示していることを確認する"
        when: "#coin を入れる"
        then: "#coin が 利用可能貨幣で #coinSlotState の場合、 金額表示機に表示されている金額に投入された紙幣の金額が加算され、返金されない"
        and: "#coin が 利用不可能貨幣 または #の場合、 金額表示機の表示に変化がなく、紙幣投入口から返金される"
        where:
        inputPath1                 | inputPath2            | outputPath     | mode           | bill               | poolMoneyIndicator | refund
        AcceptMoneyMode.value()    | AcceptBill.value()    | Accept.value() | AcceptMoney    | randomNormalBill() | AddMoney.value()   | NotRefund.value()
        NotAcceptMoneyMode.value() | AcceptBill.value()    | Refund.value() | AcceptingMoney | randomNormalBill() | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | AcceptBill.value()    | Refund.value() | SendingProduct | randomNormalBill() | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | AcceptBill.value()    | Refund.value() | NotSaleTime    | randomNormalBill() | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | AcceptBill.value()    | Refund.value() | Maintenance    | randomNormalBill() | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | AcceptBill.value()    | Refund.value() | Drawing        | randomNormalBill() | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | AcceptBill.value()    | Refund.value() | Refunding      | randomNormalBill() | NotChange.value()  | RefundFromChangeSlot.value()
        AcceptMoneyMode.value()    | NotAcceptBill.value() | Refund.value() | AcceptMoney    | randomErrorBill()  | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | NotAcceptBill.value() | Refund.value() | AcceptingMoney | randomErrorBill()  | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | NotAcceptBill.value() | Refund.value() | SendingProduct | randomErrorBill()  | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | NotAcceptBill.value() | Refund.value() | NotSaleTime    | randomErrorBill()  | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | NotAcceptBill.value() | Refund.value() | Maintenance    | randomErrorBill()  | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | NotAcceptBill.value() | Refund.value() | Drawing        | randomErrorBill()  | NotChange.value()  | RefundFromChangeSlot.value()
        NotAcceptMoneyMode.value() | NotAcceptBill.value() | Refund.value() | Refunding      | randomErrorBill()  | NotChange.value()  | RefundFromChangeSlot.value()
    }

    // 投入枚数のテスト：硬貨全種-モードランダム-投入枚数全パターン・紙幣全種-モードランダム-投入枚数全パターン
    // 投入金額のテスト：硬貨全種-モードランダム-投入金額全パターン・紙幣全種-モードランダム-投入金額全パターン
    // 金庫内枚数のテスト：紙幣全種-モードランダム-投入枚数全パターン-金庫内金額全パターン

    //TODO 合計金額を計算する
    //TODO 販売可能な商品を通知する
    //TODO 今まで受け付けた金額の表示
    //TODO 後処理
}
