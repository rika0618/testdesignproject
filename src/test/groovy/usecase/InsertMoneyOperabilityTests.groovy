package usecase

import spock.lang.Specification
import spock.lang.Unroll


/**
 * Created with IntelliJ IDEA.
 * User: k.kobayashi
 * Date: 13/09/24
 * Time: 16:58
 * To change this template use File | Settings | File Templates.
 */
class InsertMoneyOperabilityTests {

    @Unroll
    def "受け付けてほしいお金がすんなり入ること"(){
        given:
        when:
        then:
        where:
        BillType | expected
        落書きが3分の1書き込まれた紙幣 | 受け付ける
        水にぬれたものを3時間乾かした紙幣 |受け付ける
        _ランダムに30回折ったあとに伸ばした紙幣 |受け付ける
        端がやぶれた紙幣 |受け付ける
        新札 |受け付ける
        nセンチ以上切れた紙幣 | _3秒以内に返ってくる
        n平方センチ以上落書きされた紙幣 | _3秒以内に返ってくる
        水にぬれたものを30分乾かした紙幣 | _3秒以内に返ってくる
        _ランダムに5000回折ったあとに伸ばした紙幣 |_3秒以内に返ってくる
        透かしのない偽札 |_3秒以内に返ってくる
    }

    @Unroll
    def "受け付けてほしいお金が連続してすんなり入ること"(){
        given:
        when:
        then:
        where:
        _1stMoney | _2ndMoney | expected
        落書きが3分の1書き込まれた紙幣 |落書きが3分の1書き込まれた紙幣 | _1秒以内に受け付ける
        水にぬれたものを3時間乾かした紙幣 |水にぬれたものを3時間乾かした紙幣 |_1秒以内に受け付ける
        _ランダムに30回折ったあとに伸ばした紙幣 |_ランダムに30回折ったあとに伸ばした紙幣 |_1秒以内に受け付ける
        端がやぶれた紙幣 |端がやぶれた紙幣 |_1秒以内に受け付ける
        新札 |新札 |_1秒以内に受け付ける
        nセンチ以上切れた紙幣 |落書きが3分の1書き込まれた紙幣 | _1秒以内に受け付ける
        n平方センチ以上落書きされた紙幣 |水にぬれたものを3時間乾かした紙幣 |_1秒以内に受け付ける
        水にぬれたものを30分乾かした紙幣 |_ランダムに30回折ったあとに伸ばした紙幣 |_1秒以内に受け付ける
        _ランダムに5000回折ったあとに伸ばした紙幣 |端がやぶれた紙幣 |_1秒以内に受け付ける
        透かしのない偽札 |新札 |_1秒以内に受け付ける
    }
}