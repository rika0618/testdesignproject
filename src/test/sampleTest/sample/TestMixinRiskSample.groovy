package sample

import resource.Cost
import risk.Risk
import spec.BillSpec
import spec.CoinSpec
import spec.Project
import spec.SpecBase
import spock.lang.Specification

class TestMixinRiskSample extends Specification{

    def usability = new Usability()
    def functional = new Functional()

    def "なにかが正しいことを確認する"(){
        when:
        usability.operation([{println "1st"}])
        functional.operation([{println "1st"}])
        then:
        usability.Assert(UsabilitySpec.Visibility, true, {a, e -> a.value(1) == e})
        functional.Assert(BillSpec._1000, 1000, {BillSpec a, e -> a.value == e})
    }
}

//TODO リソース配下に存在する想定
class ServeTime extends Specification{
    def "スケジュールが間に合う"(){
        expect:
        new Project().validate()
    }
}

//class FSharpStudy extends Specification {
//    def "F# を勉強する"(){
//        given:"F# にバグが存在することがわかっている"
//        when:"F# の勉強する"
//        def skill = project.kyon.study(new Cost(point:10), Sprint2)
//        then:"開発を止まらないようにしたい"
//        skill >= FSharpHighSkillRisk.skill() * FSharpSkillCompromise.Middle
//    }
//}

class FSharpHighSkillRisk extends Risk{
    static int skill(){
        10
    }
}

class UsabilitySpec extends SpecBase{
    def static Visibility = new UsabilitySpec(description: "視認性", value: {視力 -> 0.5 < 視力})
}
class Usability extends Risk {
    Usability(){
        impactedSpecs = [UsabilitySpec]
    }

}

class Functional extends Risk{
    Functional(){
        impactedSpecs = [BillSpec, CoinSpec]
    }
}
