import algorithm.AlgorithmBase
import algorithm.Combination
import piece.BillSlotTests
import resource.person.Person
import risk.ResourceSolution
import risk.Risk

import risk.Solution
import risk.TestSolution
import spec.BillSlot
import spec.CoinSlot
import spec.Project
import spock.lang.Specification

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/03
 * Time: 16:18
 * To change this template use File | Settings | File Templates.
 */
class RiskApproachSample extends Specification {
    def "sample" () {
        when:
        def result = RiskApproachDescription.gen(risk, solution, algorithm)
        then:
        println result
        where:
        risk|solution|algorithm
        new RiskSample(name: "利用可能な貨幣を受け入れない", possibility: 0.9, severity: 0.8, impactedSpecs: [BillSlot, CoinSlot],solutionType: "test")| new TestSolution(name: "紙幣投入口テスト", solutionItem: BillSlotTests) | new Combination()
        new RiskSample(name: "テストドメインに詳しい人がいない", possibility: 0.9, severity: 0.4, impactedSpecs: [Project],solutionType: "resource")| new ResourceSolution(name: "担当者が学習する", solutionItem:Person) | new Combination()
    }
}

class RiskSample extends Risk{
    @Override
    def riskAssertCondition(actual, expected) {
        return null  //To change body of implemented methods use File | Settings | File Templates.
    }
}

class RiskApproachDescription{
    static String gen(Risk risk, Solution solution, AlgorithmBase algorithm){
        new RiskApproach(what: risk, how:solution, howMuch: algorithm).description()
    }
}

class RiskApproach{
    Risk what
    Solution how
    AlgorithmBase howMuch

    String description(){
        what.name + "というリスクを" + how.name + "によって" + howMuch.description() + "軽減する"
    }
}
