package sample

import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.junit.runners.model.RunnerBuilder
import piece.BillSlotTests
import piece.CoinSlotTests

/**
 * Created with IntelliJ IDEA.
 */
class Approach extends Suite{

    /**
     *  Called reflectively on classes annotated with <code>@RunWith(Suite.class)</code>
     *
     * @param klass the root class
     * @param builder builds runners for classes in the suite
     * @throws org . junit . runners . model . InitializationError
     */ Approach(Class<?> klass, RunnerBuilder builder) throws Throwable {
        super(builder, klass, klass.approaches as Class<?> [])
    }
}


@RunWith(Approach)
class RiskApproach {
    def static approaches = [
            BillSlotTests,
            CoinSlotTests
    ]
}
