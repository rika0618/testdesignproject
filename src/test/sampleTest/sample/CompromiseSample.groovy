package sample

import algorithm.Compromise
import algorithm.HogeCompromise
import spock.lang.Specification


/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/09/26
 * Time: 17:53
 * To change this template use File | Settings | File Templates.
 */
class CompromiseSample extends Specification {

    def "sample"() {
        when:
        "$risk を $plan によって $compromiseRate によって ${compromiseRate.rate()}ほど軽減させる "
        then:
        assert true
        where:
        risk | reason | compromiseRate | plan
        1 | 1 | HogeCompromise.High | "TestDomain.FooTest"
    }
}

