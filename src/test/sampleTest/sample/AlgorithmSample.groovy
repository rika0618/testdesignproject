package sample

import algorithm.Combination
import algorithm.Parameter
import groovy.transform.Canonical
import spock.lang.Specification


/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/09/26
 * Time: 15:48
 * To change this template use File | Settings | File Templates.
 */
class AlgorithmSample extends Specification {

    def "sample"(){
        when:
        def result = Al.gen([p1,p2,p3], criteria, [constraints])
        then:
        result.each{println it}
        where:
        p1 | p2 | p3 | criteria | constraints
        new Parameter(factor: "A", level: [1,2,3]) | new Parameter(factor: "B", level: [3,4]) | new Parameter(factor: "C", level: [6,7]) | Combination.Technique.All | {a,b,c, expected -> if(a == 1){ [1,4,6]} else{ expected }  }
    }

}

@Canonical
class SampleCase extends Case {
    int A
    int B
    int C
}

class Al{
    static List gen(List<Parameter> parameters, Combination.Technique criteria, List<Closure> constraints){
        // as SampleCase は何らかの形でジェネリックにしたい
        criteria.generate(parameters).collect{it as SampleCase}
    }
}
