package sample

import risk.Risk
import risk.Solution
import spec.BillSlot
import spec.CoinSlot
import spec.Project
import spec.SpecBase
import spock.lang.Specification


/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/02
 * Time: 16:11
 * To change this template use File | Settings | File Templates.
 */
class RiskSample extends Specification {

    def "sample"() {
        when:
        def result = RiskDescription.gen(type, name, possibility, severity, impactedSpecs, solutionType)
        then:
        println result
        where:
        type    | name               | possibility | severity | impactedSpecs        | solutionType
        "機能正確性" | "利用可能な貨幣を受け入れない"   | 0.9         | 0.8      | [CoinSlot, BillSlot] | "test"
        "人的資源"  | "テストドメインに詳しい人がいない" | 0.9         | 0.4      | [Project]            | "resource"
    }

    def "あるリスクをどのように解決するのかを示す。"() {
        expect:
        true
        where:
        risk                                                                         | solution | algorithm
        new RiskForThisTest(name: "", possibility: 1, severity: 1, type: "", impactedSpecs: []) | 1        | 1
    }
}


class RiskDescription {
    static String gen(String type, String name, BigDecimal possibility, BigDecimal severity, List<SpecBase> impactedSpecs, String solutionType) {
        Risk AcceptMoneyRisk = new RiskForThisTest(name: name, type: type, possibility: possibility, severity: severity, impactedSpecs: impactedSpecs, solutionType: solutionType)
        AcceptMoneyRisk.description()
    }
}

class RiskForThisTest extends Risk{

    @Override
    def riskAssertCondition(actual, expected) {
        return null  //To change body of implemented methods use File | Settings | File Templates.
    }
}
