package sample

import spock.lang.Specification
import spock.lang.Unroll


/**
 * Created with IntelliJ IDEA.
 */
class TestLevelSample extends Specification {

    @Unroll
    def "#testLevel によって #TestDomain のテストがコンパイル時もしくは実行時に制約のチェックを受けることを確認する"(){
        expect:
        // TestDomainを実行する
        where:
        testLevel | TestDomain | expected
        TestLevel.UnitTest | FooUnitTest | true
        TestLevel.IntegrationTest | FooUnitTest | false
    }

}

class Spec{
    static class UserInterface {

    }
    static     class RestAPI {

    }
    static class Log {

    }
    static class Database {

    }

}
class TestLevel {
    def wholeTestTarget = [Spec.UserInterface, Spec.Database, Spec.Log]
    def testTarget
    //なんらかの形でSpec配下のテスト対象を使う
    def static UnitTest = new TestLevel(testTarget: new Spec.Database())
    def static IntegrationTest = new TestLevel(testTarget: new Spec.UserInterface())
}
class FooUnitTest{

}