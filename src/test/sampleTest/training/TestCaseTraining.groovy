package training

import spec.Adaptability
import spec.Product
import spec.Satisfiability

import static spec.Product.*
import spec.Rack
import spec.Temperature
import spock.lang.Specification


/**
 * Created with IntelliJ IDEA.
 * User: rika
 * Date: 13/10/06
 * Time: 23:56
 * To change this template use File | Settings | File Templates.
 */
class TestCaseTraining extends Specification {
    def productSlotFunction = new ProductSlotFunction()
    def productSlotAdaptability = new ProductSlotAdaptability()

    def "外気温45度の日に適温に冷えたジュースを取り出せる"() {
        given:
        "外気温を" + Temperature.High + "度にしておく"
        when:
        productSlotFunction.operation()
        productSlotAdaptability.operation()
        then:
        productSlotFunction.Assert(Satisfiability.practicality, true, { a, e -> a.value(10) == e })
        productSlotFunction.Assert(Satisfiability.practicality, false, { a, e -> a.value(15) == e })
        productSlotAdaptability.Assert(Adaptability.ColdProductTemperatureAdaptability, true, { a, e -> a.value(8) == e })
        productSlotAdaptability.Assert(Adaptability.ColdProductTemperatureAdaptability, false, { a, e -> a.value(10) == e })
    }
}