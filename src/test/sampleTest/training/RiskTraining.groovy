package training

import spec.Adaptability
import spec.Product
import spec.Satisfiability
import spec.Temperature
import spock.lang.Specification

import risk.Risk
import spec.ProductSlot


/**
 * Created with IntelliJ IDEA.
 * User: rika
 * Date: 13/10/06
 * Time: 22:58
 * To change this template use File | Settings | File Templates.
 */
class RiskTraining extends Specification {
    Risk productSlotFunction = new ProductSlotFunction()
    Risk productSlotAdaptability = new ProductSlotAdaptability()
}

class ProductSlotFunction extends Risk {
    ProductSlotFunction() {
        name = "商品取り出し口機能"
        type = "機能性"
        possibility = 0.3
        severity = 0.8
        impactedSpecs = [Satisfiability, Product, ProductSlot]
        solutionType = "test"
    }

    def operation() {
        println "10秒以内に商品を取り出す"
    }
}

class ProductSlotAdaptability extends Risk {
    ProductSlotAdaptability() {
        name = "商品取り出し口環境適応性"
        type = "移植性"
        possibility = 0.5
        severity = 0.4
        impactedSpecs = [Adaptability, Temperature, Product, ProductSlot]
        solutionType = "test"
    }

    def operation() {
        println "商品の温度を測る"
    }
}
