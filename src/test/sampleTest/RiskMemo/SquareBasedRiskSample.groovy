package RiskMemo

import spock.lang.Specification

import static risk.FunctionalCompletenessRisk.*
import static risk.FunctionalCorrectnessRisk.*
import static risk.FunctionalAppropriateness.*

/**
 * Created with IntelliJ IDEA.
 * User: r.nakajima
 * Date: 13/10/08
 * Time: 16:22
 * To change this template use File | Settings | File Templates.
 */
class SquareBasedRiskSample extends Specification {

    def "機能完全性"() {
        where:
        risk                    | firstActor | secondActor | indirectActor
        CannotInsertMoney       | Customer   | Owner       | Vendor
        CannotKnowInsertedPrice | Customer   | Owner       | Vendor
        CannotGetChange         | Customer   | Owner       | Vendor
        CannotGetProduct        | Customer   | Owner       | Vendor
        GetAnotherProduct       | Customer   | Owner       | Vendor
        CannotWinInDrawing      | Customer   | Owner       | Vendor
        CannotGetWinProduct     | Customer   | Owner       | Vendor
        CannotDraw              | Customer   | Owner       | Vendor
        CannotSupplyChange      | Supplier   | Owner       | Vendor
        CannotSupplyProduct     | Supplier   | Owner       | Vendor
        CannotGetOutMoney       | Supplier   | Owner       | Vendor
    }

    def "機能正確性"() {
        where:
        risk                                      | firstActor | secondActor | indirectActor
        GetLessChange                             | Customer   | Owner       | Vendor
        RecognizedMoneyIsDifferentFromInsertMoney | Customer   | Owner       | Vendor
        GetTepidProduct                           | Customer   | Owner       | Vendor
    }

    def "機能適切性"() {
        where:
        risk                          | firstActor | secondActor | indirectActor
        CannotKnowWhetherPushedButton | Customer   | Owner       | Vendor
        CannotKnowWhetherDraw         | Customer   | Owner       | Vendor
        CloggedWithMoney              | Supplier   | Owner       | Vendor
        CloggedWithProduct            | Supplier   | Owner       | Vendor
    }
}
